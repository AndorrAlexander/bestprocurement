<?php
	$hp_slides_args = array( 'post_type' => 'homepage_slide');
	$hp_slides_query = new WP_Query( $hp_slides_args );
?>
<?php if( $hp_slides_query->have_posts() ): ?>
<div class="bp-hp-slider">
	<?php while( $hp_slides_query->have_posts() ): $hp_slides_query->the_post(); ?>
	<?php $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
	<div class="slide" style="background-image: url('<?php echo $full_image_url[0]; ?>')">
		<div class="wrapper">
			<div class="headline">
				<h3 class="title"><?php the_title(); ?></h3>
				<div class="text"><?php the_content(); ?></div>
				<a class="find-more" href="<?php echo get_post_meta($post->ID, 'homepage_slide_link', true) ? get_post_meta($post->ID, 'homepage_slide_link', true) : '#'; ?>" title="Find out more">Find out more</a>
			</div>
		</div>
	</div>
	<?php endwhile; ?>
</div>
<?php endif; wp_reset_query(); ?>
