<div class="inner-above-footer-wrapper">
	<div class="inner-above-footer">
		<div class="wrapper">
			<span class="question">Looking for a quotation?</span><a class="quotation" href="<?php echo get_the_permalink( get_page_by_path('contact') ); ?>" title="Contact us">Contact us</a>
		</div>
	</div>
</div>
