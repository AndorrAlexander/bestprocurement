	
	<?php get_template_part( 'template-parts/about', 'carousel' ); ?>
	
<div class="wrapper">
	<section class="about-content">

		<h1 class="hidden"></h1>
		<article class="about-articles description">
			<?php
				$id = get_the_ID();
				$subtitle = get_post_meta( $id, 'meta-box-subtitle', true );
			?>
			<h2 class="subtitle"><?php echo !empty( $subtitle ) ? $subtitle : the_title(); ?></h2>
			<div class="text"><?php the_content(); ?></div>
		</article>

		<article class="about-articles portfolio-wrapper">
		<?php if( locate_template( 'template-parts/portfolio-slider.php') != '' ): ?>
			<?php get_template_part('template-parts/portfolio', 'slider'); ?>
		<?php else: ?>
			<?php echo 'Slider missing'; ?>
		<?php endif; ?>
		</article>

	</section>
</div>
