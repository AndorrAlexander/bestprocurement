<?php
	$about_slides_args = array( 'post_type' => 'about_us_slide');
	$about_slides_query = new WP_Query( $about_slides_args );
?>
<?php if( $about_slides_query->have_posts() ): ?>
<div class="bp-about-slider">
	<?php while( $about_slides_query->have_posts() ): $about_slides_query->the_post(); ?>
	<?php $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
	<div class="slide" style="background-image: url('<?php echo $full_image_url[0]; ?>')">&nbsp;</div>
	<?php endwhile; ?>
</div>
<?php endif; wp_reset_query(); ?>
