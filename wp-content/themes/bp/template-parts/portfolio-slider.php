<?php
	$portfolio_args = array( 'post_type' => 'portfolio', 'posts_per_page' => -1 );
	$portfolio_query = new WP_Query( $portfolio_args );
?>
<?php if( $portfolio_query->have_posts() ): ?>
	<h2 class="title"><?php _e('Portfolio', 'bp'); ?></h2>
	<div class="portfolio-articles">
	<?php while( $portfolio_query->have_posts() ): $portfolio_query->the_post(); ?>
		<?php if( get_post_meta( $post->ID, 'portfolio_slide', true ) == true ): ?>
		<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' ); ?>
		<article class="portfolio" style="background-image: url('<?php echo $image_url[0]; ?>');">
			<span class="title"><?php the_title(); ?></span>
		</article>
		<?php endif; ?>
	<?php endwhile; ?>
	</div>
<?php endif; wp_reset_query(); ?>
