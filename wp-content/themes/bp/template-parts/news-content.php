<?php
	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
	$big = 999999999; // need an unlikely integer
	$news_args = array( 
		'post_type' => 'news',
		'paged' => $paged,
	);
	$news_query = new WP_Query( $news_args );
	$pagination_args = array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format'             => '?page=%#%',
		'total'              => 1,
		'current'            => 0,
		'show_all'           => true,
		'end_size'           => 1,
		'mid_size'           => 2,
		'prev_next'          => true,
		'prev_text'          => __('« Previous'),
		'next_text'          => __('Next »'),
		'type'               => 'plain',
		'add_args'           => true,
		'add_fragment'       => '',
		'before_page_number' => '',
		'after_page_number'  => '',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $news_query->max_num_pages
	);
?>
	<section class="news">
		<div class="wrapper">
		<?php if( $news_query->have_posts() ): ?>
			<?php while( $news_query->have_posts() ): $news_query->the_post(); ?>
			<article class="news-item">
				<?php $image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'medium' ); ?>
				<a class="thumb-wrapper" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" style="background-image: url('<?php echo $image_url[0]; ?>');">&nbsp;</a>
				<h2 class="title">
					<a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>"><?php echo get_the_title(); ?></a>
				</h2>
				<div class="text"><?php echo get_the_excerpt() > 0 ? content_excerpt( get_the_excerpt(), 200, 'chars' ) : content_excerpt( get_the_content(), 200, 'chars' ); ?></div>
				<a class="read-more" href="<?php the_permalink(); ?>" title="Read more about <?php the_title(); ?>"><?php _e('Read More', 'bp'); ?></a>
			</article>
			<?php endwhile; ?>
		<?php endif; wp_reset_query(); ?>
			<div class="pagination">
			<?php echo paginate_links( $pagination_args ); ?>
			</div>
		</div>
	</section>