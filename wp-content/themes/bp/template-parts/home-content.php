<div class="wrapper">
	<section class="hp-content">

		<h1 class="hidden"></h1>

		<article class="hp-articles description">
			<h2 class="title"><?php _e('Description', 'bp'); ?></h2>
			<div class="text"><?php echo theme_option('homepage_description'); ?></div>
		</article>

		<div class="hp-articles news-wrapper">
		<?php
			$news_args = array( 'post_type' => 'news', 'posts_per_page' => 3);
			$news_query = new WP_Query( $news_args );
		?>
		<?php if( $news_query->have_posts() ): ?>
			<h2 class="title"><?php _e('Recent news', 'bp'); ?></h2>
			<div class="news-articles">
				<?php while( $news_query->have_posts() ): $news_query->the_post(); ?>
				<article class="news">
					<h3 class="title"><a href="<?php the_permalink(); ?>" title="<?php _e('Read more', 'bp'); ?>"><?php the_title(); ?></a></h3>
					<div class="excerpt"><?php the_content(); ?></div>
				</article>
				<?php endwhile; ?>
			</div>
		<?php endif; wp_reset_query(); ?>
		</div>

		<div class="hp-articles portfolio-wrapper">
		<?php if( locate_template( 'template-parts/portfolio-slider.php') != '' ): ?>
			<?php get_template_part('template-parts/portfolio', 'slider'); ?>
		<?php else: ?>
			<?php echo 'Slider missing'; ?>
		<?php endif; ?>
		</div>

	</section>
</div>
