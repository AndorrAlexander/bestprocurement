<?php
	$services_args = array( 'post_type' => 'service', 'posts_per_page' => -1);
	$services_query = new WP_Query( $services_args );
?>
<?php if( $services_query->have_posts() ): ?>

<div class="services">
	<?php $i=0; while( $services_query->have_posts() ): $services_query->the_post(); $i++; ?>
	<?php $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
	<article class="service-item<?php the_slug(true); ?><?php echo $i == 1 ? ' current': ''; ?>" <?php if( has_post_thumbnail() ): ?>style="background-image: url('<?php echo $full_image_url[0]; ?>');"<?php endif; ?> data-tab="<?php tab_data(true); ?>">
		<h3 class="service-title"><?php the_title(); ?></h3>
	</article>
	<?php endwhile; ?>
</div>

<section class="service-page-content">
	<?php $i=0; while( $services_query->have_posts() ): $services_query->the_post(); $i++; ?>
	<div id="<?php tab_data(true); ?>" class="wrapper <?php echo $i == 1 ? 'current': ''; ?>">
		<?php the_content(); ?>
	</div>
	<?php endwhile; ?>
</section>
<?php endif; wp_reset_query(); ?>

