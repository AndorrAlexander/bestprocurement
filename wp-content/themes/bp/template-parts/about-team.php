<?php
	$member_args = array( 
		'post_type' => 'team_member', 
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order' => 'ASC'
	);
	$members_query = new WP_Query( $member_args );
?>

<?php if( $members_query->have_posts() ): ?>
<section class="team">
	<h2 class="title"><?php _e('Who we are', 'bp'); ?></h2>
	<div class="team-members">
	<?php while( $members_query->have_posts() ): $members_query->the_post(); ?>
		<article class="team-member">
		<?php if( has_post_thumbnail() ): ?>
			<figure class="team-member-thumb">
			<?php the_post_thumbnail('team-member-thumb'); ?>
			</figure>
		<?php endif; ?>
			<h3 class="name"><?php the_title(); ?></h3>
			<span class="position"><?php echo get_post_meta( $post->ID, 'member_position', true );?></span>
			<div class="excerpt"><?php the_content(); ?></div>
		</article>
	<?php endwhile;?>
	</div>
</section>
<?php endif; wp_reset_query(); ?>
