<?php
	$industry_args = array( 'post_type' => 'industry', 'posts_per_page' => 6, 'order' => 'ASC');
	$industry_query = new WP_Query( $industry_args );
?>
<?php if( $industry_query->have_posts() ): ?>
	<div class="industries wrapper">
		<?php $i=0; while( $industry_query->have_posts() ): $industry_query->the_post(); $i++; ?>
		<?php $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
		<article class="industry-item<?php the_slug(true); ?> <?php echo $i == 1 ? ' current': ''; ?>" <?php if( has_post_thumbnail() ): ?>style="background-image: url('<?php echo $full_image_url[0]; ?>');"<?php endif; ?> data-tab="<?php tab_data(true); ?>">
			<h3 class="industry-title"><?php the_title(); ?></h3>
		</article>
		<?php endwhile; ?>
	</div>
	
<section class="industry-page-content">
	<div class="wrapper">
	<?php $i=0; while( $industry_query->have_posts() ): $industry_query->the_post(); $i++; ?>
		<div id="<?php tab_data(true); ?>" class="industry-content<?php echo $i == 1 ? ' current': ''; ?>">
			<h1 class="title"><?php echo the_title(); ?></h1>
			<div><?php the_content(); ?></div>
		</div>
	<?php endwhile; ?>
	</div>
</section> 
<?php endif; wp_reset_query(); ?>