jQuery(document).ready(function($){

	$('.bp-hp-slider').slick({
		autoplay: true,
		dots: true,
		infinite: true,
		speed: 500,
		fade: true,
		cssEase: 'linear',
		prevArrow: '',
		nextArrow: '',
	});
	
	$('.partners .list').slick({
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 2,
		autoplay: false,
		autoplaySpeed: 2000,
		responsive: [
			{
				breakpoint: 768,
				settings: {
					arrows: true,
					slidesToShow: 4,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 500,
				settings: {
					arrows: true,
					slidesToShow: 2,
					slidesToScroll: 1
				}
			}
		]
	});
	
	$('.bp-about-slider').slick({
		dots: true,
		infinite: true,
		speed: 500,
		fade: true,
		cssEase: 'linear',
		prevArrow: '',
		nextArrow: '',
	});

	$('.news-articles').slick({
		dots: true,
		infinite: true,
		speed: 500,
		fade: false,
		cssEase: 'linear',
		prevArrow: '',
		nextArrow: '',
	});

	$('.portfolio-articles').slick({
		autoplay: true,
		arrows: true,
		dots: false,
		infinite: true,
		speed: 500,
		fade: true,
		cssEase: 'linear',
	});

	var center = function(element){
		var h = element.height(),
			top = h/2,
			w = element.width(),
			left = w/2;

		element.css('margin-top', -top);
		element.css('margin-left', -left);
	}

	var center_less = function(element){
		var h = element.height(),
			top = h/4,
			w = element.width(),
			left = w/2;

		element.css('margin-top', -4*top);
		element.css('margin-left', -left);
	}

	var title = $('.header-title.lp h2.title');
	if( title.length ) {
		center( title );
	}

	var big_title = $('.header-title.big h2.title');
	if( big_title.length ) {
		center_less( big_title );

		$(window).resize(function(){
			center_less( big_title );
		});
	}

	
	$('.page-template-page-services .services .service-item').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('.page-template-page-services .services .service-item').removeClass('current');
		$('.service-page-content .wrapper').fadeOut(500, function(){
			$('.service-page-content .wrapper').removeClass('current');
		});

		$(this).addClass('current');
		$("#" + tab_id).fadeIn(500, function(){
			$("#" + tab_id).addClass('current');
		});
	});
	
	
	$('.page-template-page-industry .industries .industry-item').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('.page-template-page-industry .industries .industry-item').removeClass('current');
		$('.industry-page-content .industry-content').fadeOut(300, function(){
			$('.industry-page-content .industry-content').removeClass('current');
		});

		$(this).addClass('current');
		$("#" + tab_id).fadeIn(400, function(){
			$("#" + tab_id).addClass('current');
		});
		
//		$('html,body').delay(400).animate({
//			scrollTop: $("#" + tab_id).offset().top
//		}, 'slow');
	});
	
	$('.portfolio-item').on('touchstart mouseenter focus', function(e) {
		if(e.type == 'touchstart') {
			// Don't trigger mouseenter even if they hold
			e.stopImmediatePropagation();
			// If $item is a link (<a>), don't go to said link on mobile, show menu instead
			e.preventDefault();
		}
	});
});

