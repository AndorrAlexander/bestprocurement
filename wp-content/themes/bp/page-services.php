<?php
	/**
	 * Template name: Services page
	 */
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/inner', 'header-services'); ?>

<?php get_template_part('template-parts/service', 'content'); ?>

<?php get_template_part('template-parts/inner', 'above-footer'); ?>

<?php get_footer(); ?>




