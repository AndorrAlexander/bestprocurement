<?php
	/**
	 * Template name: About page
	 */
?>

<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<?php get_template_part( 'template-parts/about', 'content' ); ?>
		</main>
	</div>

<?php get_template_part('template-parts/inner', 'above-footer'); ?>

<?php get_footer(); ?>




