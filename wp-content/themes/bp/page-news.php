<?php
	/**
	 * Template name: News page
	 */
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/inner', 'header'); ?>

<?php get_template_part('template-parts/news', 'content'); ?>

<?php get_template_part('template-parts/inner', 'above-footer'); ?>

<?php get_footer(); ?>
