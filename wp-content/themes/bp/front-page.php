<?php get_header(); ?>

		<?php get_template_part( 'template-parts/home', 'carousel' ); ?>
		<?php get_template_part( 'template-parts/home', 'content' ); ?>
		<?php get_template_part( 'template-parts/home', 'partners' ); ?>

<?php get_footer(); ?>


