<?php
/**
 * bestprocurement functions and definitions
 *
 * @package bestprocurement
 */


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( !isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}


if ( !function_exists( 'bp_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
	function bp_setup() {

		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on bestprocurement, use a find and replace
		 * to change 'bp' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'bp', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'bp' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
		) );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'aside', 'image', 'video', 'quote', 'link',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'bp_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
	}
	add_action( 'after_setup_theme', 'bp_setup' );
endif; // bp_setup


/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
if( !function_exists('bp_widgets_init') ):
	function bp_widgets_init() {
		register_sidebar( array(
			'name'          => __( 'Footer Description', 'bp' ),
			'id'            => 'footer-description',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		) );

		register_sidebar( array(
			'name'          => __( 'Footer Newsletter', 'bp' ),
			'id'            => 'footer-newsletter',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		) );

		register_sidebar( array(
			'name'          => __( 'Footer Contacts', 'bp' ),
			'id'            => 'footer-contacts',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		) );

		register_sidebar( array(
			'name'          => __( 'Footer Social', 'bp' ),
			'id'            => 'footer-social',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		) );
	}
	add_action( 'widgets_init', 'bp_widgets_init' );
endif;


/**
 * Enqueue Website scripts and styles.
 */
if( !function_exists('bp_site_scripts') ):
	add_action( 'wp_enqueue_scripts', 'bp_site_scripts' );
	function bp_site_scripts() {
		$d = get_template_directory_uri();
		
		wp_enqueue_style( 'slick-style', $d . '/inc/vendors/slick/slick.css' );

		wp_enqueue_script( 'fonts', $d . '/js/fonts.js', array(), '1.0', true );
		wp_enqueue_script( 'bp-navigation',  $d . '/js/navigation.js', array(), '20120206', true );
		wp_enqueue_script( 'bp-skip-link-focus-fix', $d . '/js/skip-link-focus-fix.js', array(), '20130115', true );
		wp_enqueue_script( 'slick-js', $d . '/inc/vendors/slick/slick.min.js', array(), '1.5.0', true );

		if( is_single() ) {
			wp_enqueue_style( 'swipebox-style', $d . '/inc/vendors/swipebox/css/swipebox.min.css' );
			wp_enqueue_script( 'swipebox-js', $d . '/inc/vendors/swipebox/js/jquery.swipebox.min.js', array(), '1.4.1', true );
		}
		
		wp_enqueue_style( 'bp-style', get_stylesheet_uri() );

		wp_enqueue_script( 'main-js', $d . '/js/main.js', array(), '1.0', true );
	}
endif;


/**
 * Enqueue Administration scripts and styles.
 */
if( !function_exists('bp_admin_scripts') ):
	add_action('admin_enqueue_scripts', 'bp_admin_scripts');
	function bp_admin_scripts() {
		wp_enqueue_style('bp-admin-theme', get_template_directory_uri() . '/layouts/bp-admin.css');
	}
endif;


/**
 * Register News post type.
 */
if( !function_exists('news_init') ):
	add_action( 'init', 'news_init' );
	function news_init() {
		$labels = array(
			'name'               => _x( 'News', 'post type general name', 'bp' ),
			'singular_name'      => _x( 'News', 'post type singular name', 'bp' ),
			'menu_name'          => _x( 'BP News', 'admin menu', 'bp' ),
			'name_admin_bar'     => _x( 'News', 'add new on admin bar', 'bp' ),
			'add_new'            => _x( 'Add new News', 'news', 'bp' ),
			'add_new_item'       => __( 'Add new News', 'bp' ),
			'new_item'           => __( 'New news', 'bp' ),
			'edit_item'          => __( 'Edit news', 'bp' ),
			'view_item'          => __( 'View news', 'bp' ),
			'all_items'          => __( 'All News', 'bp' ),
			'search_items'       => __( 'Search news', 'bp' ),
			'parent_item_colon'  => __( 'Parent news:', 'bp' ),
			'not_found'          => __( 'No news found.', 'bp' ),
			'not_found_in_trash' => __( 'No news found in Trash.', 'bp' )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'news' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'menu_icon'			 => 'dashicons-media-document',
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
		);

		register_post_type( 'news', $args );
	}
endif;


/**
 * Register Portfolio post type.
 */
if( !function_exists('portfolio_init') ):
	add_action( 'init', 'portfolio_init' );
	function portfolio_init() {
		$labels = array(
			'name'               => _x( 'Portfolio', 'post type general name', 'bp' ),
			'singular_name'      => _x( 'Portfolio', 'post type singular name', 'bp' ),
			'menu_name'          => _x( 'BP Portfolio', 'admin menu', 'bp' ),
			'name_admin_bar'     => _x( 'Portfolio', 'add new on admin bar', 'bp' ),
			'add_new'            => _x( 'Add new Portfolio item', 'portfolio', 'bp' ),
			'add_new_item'       => __( 'Add new Portfolio item', 'bp' ),
			'new_item'           => __( 'New Portfolio item', 'bp' ),
			'edit_item'          => __( 'Edit Portfolio item', 'bp' ),
			'view_item'          => __( 'View Portfolio items', 'bp' ),
			'all_items'          => __( 'All Portfolio items', 'bp' ),
			'search_items'       => __( 'Search Portfolio items', 'bp' ),
			'parent_item_colon'  => __( 'Parent Portfolio:', 'bp' ),
			'not_found'          => __( 'No Portfolio items found.', 'bp' ),
			'not_found_in_trash' => __( 'No Portfolio items found in Trash.', 'bp' )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'portfolio' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'menu_icon'			 => 'dashicons-portfolio',
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
			'register_meta_box_cb' => 'add_portfolio_slide_metabox'
		);

		register_post_type( 'portfolio', $args );
	}
	
	function portfolio_slide_markup($object) {
		wp_nonce_field(basename(__FILE__), "portfolio_slide_meta_noncename");
		$portfolio_slide = get_post_meta($object->ID, 'portfolio_slide', true);

		$markup = '';
		if( $portfolio_slide == '' ){
			$markup .= '<input type="checkbox" id="portfolio_slide" name="portfolio_slide" value="true">';
		}
		else if( $portfolio_slide == 'true' ){
			$markup .= '<input type="checkbox" id="portfolio_slide" name="portfolio_slide" value="true" checked>';
		}
		$markup .= '<label for="portfolio_slide">Include in Portfolio Slider</label>';
		
		echo $markup;
	}

	add_action('save_post', 'save_portfolio_slide_meta', 10, 3);
	function save_portfolio_slide_meta($post_id, $post, $update) {
		if( !isset( $_POST['portfolio_slide_meta_noncename'] ) || !wp_verify_nonce($_POST["portfolio_slide_meta_noncename"], basename(__FILE__)) ) {
			return $post_id;
		}
		if( !current_user_can("edit_post", $post_id) )
			return $post_id;

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$portfolio_slide_meta['portfolio_slide'] = $_POST['portfolio_slide'];

		foreach( $portfolio_slide_meta as $key => $value ) {
			if( $post->post_type == 'revision' ) return;

			$value = implode(',', (array)$value);

			if( get_post_meta($post->ID, $key, FALSE) ) {
				update_post_meta($post->ID, $key, $value);
			} else {
				add_post_meta($post->ID, $key, $value);
			}
			if( !$value ) delete_post_meta( $post->ID, $key );
		}
	}

	function add_portfolio_slide_metabox() {
		add_meta_box('portfolio_slide_markup', 'Portfolio slider', 'portfolio_slide_markup', 'portfolio', 'side', 'default');
	}
	add_action("add_meta_boxes", "add_portfolio_slide_metabox");
endif;	


/**
 * Register Homepage slide post type.
 */
if( !function_exists('homepage_slide_init') ):
	add_action( 'init', 'homepage_slide_init' );
	function homepage_slide_init() {
		$labels = array(
			'name'               => _x( 'Homepage slide', 'post type general name', 'bp' ),
			'singular_name'      => _x( 'Homepage slide', 'post type singular name', 'bp' ),
			'menu_name'          => _x( 'BP Homepage slide', 'admin menu', 'bp' ),
			'name_admin_bar'     => _x( 'Homepage slide', 'add new on admin bar', 'bp' ),
			'add_new'            => _x( 'Add new Homepage slide', 'portfolio', 'bp' ),
			'add_new_item'       => __( 'Add new Homepage slide', 'bp' ),
			'new_item'           => __( 'New Homepage slide', 'bp' ),
			'edit_item'          => __( 'Edit Homepage slide', 'bp' ),
			'view_item'          => __( 'View Homepage slide', 'bp' ),
			'all_items'          => __( 'All Homepage slides', 'bp' ),
			'search_items'       => __( 'Search Homepage slides', 'bp' ),
			'parent_item_colon'  => __( 'Parent Homepage slide:', 'bp' ),
			'not_found'          => __( 'No Homepage slides found.', 'bp' ),
			'not_found_in_trash' => __( 'No Homepage slides found in Trash.', 'bp' )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'homepage_slide' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'menu_icon'			 => 'dashicons-images-alt2',
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
		);

		register_post_type( 'homepage_slide', $args );
	}

	function homepage_slide_link_markup($object) {
		wp_nonce_field(basename(__FILE__), "homepage_slide_link_meta_noncename");
		$homepage_slide_link = get_post_meta($object->ID, 'homepage_slide_link', true);

		$markup = '';
		
		$markup .= '<textarea id="homepage_slide_link" name="homepage_slide_link" style="width: 100%;" rows="3">' . $homepage_slide_link . '</textarea>';
		
		echo $markup;
	}

	add_action('save_post', 'save_homepage_slide_link_meta', 10, 3);
	function save_homepage_slide_link_meta($post_id, $post, $update) {
		if( !isset( $_POST['homepage_slide_link_meta_noncename'] ) || !wp_verify_nonce($_POST["homepage_slide_link_meta_noncename"], basename(__FILE__)) ) {
			return $post_id;
		}
		if( !current_user_can("edit_post", $post_id) )
			return $post_id;

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$homepage_slide_link_meta['homepage_slide_link'] = $_POST['homepage_slide_link'];

		foreach( $homepage_slide_link_meta as $key => $value ) {
			if( $post->post_type == 'revision' ) return;

			$value = implode(',', (array)$value);

			if( get_post_meta($post->ID, $key, FALSE) ) {
				update_post_meta($post->ID, $key, $value);
			} else {
				add_post_meta($post->ID, $key, $value);
			}
			if( !$value ) delete_post_meta( $post->ID, $key );
		}
	}

	function add_homepage_slide_link_metabox() {
		add_meta_box('homepage_slide_link_markup', 'Homepage slide link', 'homepage_slide_link_markup', 'homepage_slide', 'side', 'default');
	}
	add_action("add_meta_boxes", "add_homepage_slide_link_metabox");
endif;


/**
 * Register AboutUs slide post type.
 */
if( !function_exists('aboutus_slide_init') ):
	add_action( 'init', 'aboutus_slide_init' );
	function aboutus_slide_init() {
		$labels = array(
			'name'               => _x( 'AboutUs slide', 'post type general name', 'bp' ),
			'singular_name'      => _x( 'AboutUs slide', 'post type singular name', 'bp' ),
			'menu_name'          => _x( 'BP AboutUs slide', 'admin menu', 'bp' ),
			'name_admin_bar'     => _x( 'AboutUs slide', 'add new on admin bar', 'bp' ),
			'add_new'            => _x( 'Add new AboutUs slide', 'portfolio', 'bp' ),
			'add_new_item'       => __( 'Add new AboutUs slide', 'bp' ),
			'new_item'           => __( 'New AboutUs slide', 'bp' ),
			'edit_item'          => __( 'Edit AboutUs slide', 'bp' ),
			'view_item'          => __( 'View AboutUs slide', 'bp' ),
			'all_items'          => __( 'All AboutUs slides', 'bp' ),
			'search_items'       => __( 'Search AboutUs slides', 'bp' ),
			'parent_item_colon'  => __( 'Parent AboutUs slide:', 'bp' ),
			'not_found'          => __( 'No AboutUs slides found.', 'bp' ),
			'not_found_in_trash' => __( 'No AboutUs slides found in Trash.', 'bp' )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'about_us_slide' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'menu_icon'			 => 'dashicons-images-alt',
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
		);

		register_post_type( 'about_us_slide', $args );
	}
endif;


/**
 * Register Team Member post type.
 */
if( !function_exists('team_member_init') ):
	add_action( 'init', 'team_member_init' );
	function team_member_init() {
		$labels = array(
			'name'               => _x( 'Team Member', 'post type general name', 'bp' ),
			'singular_name'      => _x( 'Team Member', 'post type singular name', 'bp' ),
			'menu_name'          => _x( 'BP Team Member', 'admin menu', 'bp' ),
			'name_admin_bar'     => _x( 'Team Member', 'add new on admin bar', 'bp' ),
			'add_new'            => _x( 'Add Team Member', 'portfolio', 'bp' ),
			'add_new_item'       => __( 'Add Team Member', 'bp' ),
			'new_item'           => __( 'New Team Member', 'bp' ),
			'edit_item'          => __( 'Edit Team Member', 'bp' ),
			'view_item'          => __( 'View Team Member', 'bp' ),
			'all_items'          => __( 'All Team Members', 'bp' ),
			'search_items'       => __( 'Search Team Members', 'bp' ),
			'parent_item_colon'  => __( 'Parent Team Member:', 'bp' ),
			'not_found'          => __( 'No Team Members found.', 'bp' ),
			'not_found_in_trash' => __( 'No Team Member found in Trash.', 'bp' )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'exclude_from_search'=> false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'team_member' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'menu_icon'			 => 'dashicons-businessman',
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
			'register_meta_box_cb' => 'add_team_member_position_metabox'
		);

		register_post_type( 'team_member', $args );
	}

	function team_member_position_markup($object) {
		wp_nonce_field(basename(__FILE__), "team_member_meta_noncename");
		$position = get_post_meta($object->ID, 'member_position', true);

		echo '<input type="text" name="member_position" value="' . esc_attr( $position )  . '" class="widefat" />';
	}

	add_action('save_post', 'save_member_position_meta', 10, 3);
	function save_member_position_meta($post_id, $post, $update) {

		if( !isset( $_POST['team_member_meta_noncename'] ) || !wp_verify_nonce($_POST["team_member_meta_noncename"], basename(__FILE__)) ) {
			return $post_id;
		}
		if( !current_user_can("edit_post", $post_id) )
			return $post_id;

		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		$position_meta['member_position'] = $_POST['member_position'];

		foreach( $position_meta as $key => $value ) {
			if( $post->post_type == 'revision' ) return;

			$value = implode(',', (array)$value);

			if( get_post_meta($post->ID, $key, FALSE) ) {
				update_post_meta($post->ID, $key, $value);
			} else {
				add_post_meta($post->ID, $key, $value);
			}
			if( !$value ) delete_post_meta( $post->ID, $key );
		}
	}

	function add_team_member_position_metabox() {
		add_meta_box('team_member_position_markup', 'Team Member Position', 'team_member_position_markup', 'team_member', 'side', 'default');
	}
	add_action("add_meta_boxes", "add_team_member_position_metabox");
endif;


/**
 * Register Services post type.
 */
if( !function_exists('services_init') ):
	add_action( 'init', 'services_init' );
	function services_init() {
		$labels = array(
			'name'               => _x( 'Services', 'post type general name', 'bp' ),
			'singular_name'      => _x( 'Service', 'post type singular name', 'bp' ),
			'menu_name'          => _x( 'BP Services', 'admin menu', 'bp' ),
			'name_admin_bar'     => _x( 'Services', 'add new on admin bar', 'bp' ),
			'add_new'            => _x( 'Add new Service', 'portfolio', 'bp' ),
			'add_new_item'       => __( 'Add new Service', 'bp' ),
			'new_item'           => __( 'New Service', 'bp' ),
			'edit_item'          => __( 'Edit Service', 'bp' ),
			'view_item'          => __( 'View Service', 'bp' ),
			'all_items'          => __( 'All Services', 'bp' ),
			'search_items'       => __( 'Search Services', 'bp' ),
			'parent_item_colon'  => __( 'Parent Service:', 'bp' ),
			'not_found'          => __( 'No Service found.', 'bp' ),
			'not_found_in_trash' => __( 'No Service found in Trash.', 'bp' )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'exclude_from_search'=> false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'service' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'menu_icon'			 => 'dashicons-hammer',
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
		);

		register_post_type( 'service', $args );
	}
endif;

/**
 * Register Industry post type.
 */
if( !function_exists('industry_init') ):
	add_action( 'init', 'industry_init' );
	function industry_init() {
		$labels = array(
			'name'               => _x( 'Industry', 'post type general name', 'bp' ),
			'singular_name'      => _x( 'Industry', 'post type singular name', 'bp' ),
			'menu_name'          => _x( 'BP Industries', 'admin menu', 'bp' ),
			'name_admin_bar'     => _x( 'Industries', 'add new on admin bar', 'bp' ),
			'add_new'            => _x( 'Add new Industry', 'portfolio', 'bp' ),
			'add_new_item'       => __( 'Add new Industry', 'bp' ),
			'new_item'           => __( 'New Industry', 'bp' ),
			'edit_item'          => __( 'Edit Industry', 'bp' ),
			'view_item'          => __( 'View Industry', 'bp' ),
			'all_items'          => __( 'All Industries', 'bp' ),
			'search_items'       => __( 'Search Industries', 'bp' ),
			'parent_item_colon'  => __( 'Parent Industry:', 'bp' ),
			'not_found'          => __( 'No Industry found.', 'bp' ),
			'not_found_in_trash' => __( 'No Industry found in Trash.', 'bp' )
		);

		$args = array(
			'labels'             => $labels,
			'public'             => true,
			'publicly_queryable' => true,
			'exclude_from_search'=> false,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => array( 'slug' => 'industry' ),
			'capability_type'    => 'post',
			'has_archive'        => true,
			'menu_icon'			 => 'dashicons-building',
			'hierarchical'       => true,
			'menu_position'      => null,
			'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
		);

		register_post_type( 'industry', $args );
	}
endif;


/**
 * Custom Meta Box - Page Subtitle.
 */
if( !function_exists('add_subtitle_meta_box') ):
	function subtitle_meta_box_markup($object) {
		wp_nonce_field(basename(__FILE__), "meta-box-nonce");?>
		<div>
			<input class="widefat" name="meta-box-subtitle" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-subtitle", true); ?>">
		</div>
	<?php
	}

	add_action("save_post", "save_subtitle_meta_box", 10, 3);
	function save_subtitle_meta_box($post_id, $post, $update){
		if( !isset($_POST["meta-box-nonce"]) || !wp_verify_nonce($_POST["meta-box-nonce"], basename(__FILE__)) )
			return $post_id;

		if( !current_user_can("edit_post", $post_id) )
			return $post_id;

		if( defined("DOING_AUTOSAVE") && DOING_AUTOSAVE )
			return $post_id;

		$slug = "page";
		if( $slug != $post->post_type )
			return $post_id;

		$meta_box_subtitle_value = "";

		if( isset($_POST["meta-box-subtitle"]) ) {
			$meta_box_subtitle_value = $_POST["meta-box-subtitle"];
		}
		update_post_meta($post_id, "meta-box-subtitle", $meta_box_subtitle_value);
	}

	add_action("add_meta_boxes", "add_subtitle_meta_box");
	function add_subtitle_meta_box() {
		add_meta_box("subtitle-meta-box", "Page subtitle", "subtitle_meta_box_markup", "page", "advanced", "high", null);
	}
endif;


/**
 * Custom Meta Boxes - Only for INDUSTRY page.
 */

/* Industry 1 */
if( !function_exists('add_industry_meta_box_1') ):

	function industry_meta_box_markup_1($object) {
		wp_nonce_field(basename(__FILE__), "meta-box-nonce-1");
	?>
		<div>
			<input class="widefat" name="meta-box-industry-1" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-industry-1", true); ?>">
		</div>
	<?php
	}

	add_action("save_post", "save_industry_meta_box_1", 10, 3);
	function save_industry_meta_box_1($post_id, $post, $update){
		if( !isset($_POST["meta-box-nonce-1"]) || !wp_verify_nonce($_POST["meta-box-nonce-1"], basename(__FILE__)) )
			return $post_id;

		if( !current_user_can("edit_post", $post_id) )
			return $post_id;

		if( defined("DOING_AUTOSAVE") && DOING_AUTOSAVE )
			return $post_id;

		$slug = "page";
		if( $slug != $post->post_type )
			return $post_id;

		$meta_box_industry_value_1 = "";

		if( isset($_POST["meta-box-industry-1"]) ) {
			$meta_box_industry_value_1 = $_POST["meta-box-industry-1"];
		}

		update_post_meta($post_id, "meta-box-industry-1", $meta_box_industry_value_1);
	}

	$post_id = isset( $_GET['post'] ) ? $_GET['post'] : '';
	if( is_admin() && $post_id == '16' ):
		add_action("add_meta_boxes", "add_industry_meta_box_1");
		function add_industry_meta_box_1() {
			add_meta_box("industry-meta-box-1", "Industry 1", "industry_meta_box_markup_1", "page", "advanced", "high", null);
		}
	endif;

endif; //industry1



/* Industry 2 */
if( !function_exists('add_industry_meta_box_2') ):

	function industry_meta_box_markup_2($object) {
		wp_nonce_field(basename(__FILE__), "meta-box-nonce-2");
	?>
		<div>
			<input class="widefat" name="meta-box-industry-2" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-industry-2", true); ?>">
		</div>
	<?php
	}

	add_action("save_post", "save_industry_meta_box_2", 10, 3);
	function save_industry_meta_box_2($post_id, $post, $update){
		if( !isset($_POST["meta-box-nonce-2"]) || !wp_verify_nonce($_POST["meta-box-nonce-2"], basename(__FILE__)) )
			return $post_id;

		if( !current_user_can("edit_post", $post_id) )
			return $post_id;

		if( defined("DOING_AUTOSAVE") && DOING_AUTOSAVE )
			return $post_id;

		$slug = "page";
		if( $slug != $post->post_type )
			return $post_id;

		$meta_box_industry_value_2 = "";

		if( isset($_POST["meta-box-industry-2"]) ) {
			$meta_box_industry_value_2 = $_POST["meta-box-industry-2"];
		}

		update_post_meta($post_id, "meta-box-industry-2", $meta_box_industry_value_2);
	}

	$post_id = isset( $_GET['post'] ) ? $_GET['post'] : '';
	if( is_admin() && $post_id == '16' ):
		add_action("add_meta_boxes", "add_industry_meta_box_2");
		function add_industry_meta_box_2() {
			add_meta_box("industry-meta-box-2", "Industry 2", "industry_meta_box_markup_2", "page", "advanced", "high", null);
		}
	endif;

endif;//industry2


/* Industry 3 */
if( !function_exists('add_industry_meta_box_3') ):

	function industry_meta_box_markup_3($object) {
		wp_nonce_field(basename(__FILE__), "meta-box-nonce-3");
	?>
		<div>
			<input class="widefat" name="meta-box-industry-3" type="text" value="<?php echo get_post_meta($object->ID, "meta-box-industry-3", true); ?>">
		</div>
	<?php
	}

	add_action("save_post", "save_industry_meta_box_3", 10, 3);
	function save_industry_meta_box_3($post_id, $post, $update){
		if( !isset($_POST["meta-box-nonce-3"]) || !wp_verify_nonce($_POST["meta-box-nonce-3"], basename(__FILE__)) )
			return $post_id;

		if( !current_user_can("edit_post", $post_id) )
			return $post_id;

		if( defined("DOING_AUTOSAVE") && DOING_AUTOSAVE )
			return $post_id;

		$slug = "page";
		if( $slug != $post->post_type )
			return $post_id;

		$meta_box_industry_value_3 = "";

		if( isset($_POST["meta-box-industry-3"]) ) {
			$meta_box_industry_value_3 = $_POST["meta-box-industry-3"];
		}

		update_post_meta($post_id, "meta-box-industry-3", $meta_box_industry_value_3);
	}

	$post_id = isset( $_GET['post'] ) ? $_GET['post'] : '';
	if( is_admin() && $post_id == '16' ):
		add_action("add_meta_boxes", "add_industry_meta_box_3");
		function add_industry_meta_box_3() {
			add_meta_box("industry-meta-box-3", "Industry 3", "industry_meta_box_markup_2", "page", "advanced", "high", null);
		}
	endif;

endif;//industry3



/**
 * Custom thumbnail size.
 */
if( !function_exists('bp_theme_setup') ):
	add_action( 'after_setup_theme', 'bp_theme_setup' );
	function bp_theme_setup() {
		add_image_size( 'homepage-thumb', 280, 160, false );
		add_image_size( 'portfolio-thumb', 305, 185, false );
		add_image_size( 'team-member-thumb', 214, 162, false );
		add_image_size( 'single-thumb', 214, 162, false );
	}
endif;


/**
 * Custom excerpt - can be used to truncate the content of a post if the excerpt is not set.
 *
 * @param  			$text The source of the text.
 * @param integer | $length The limit of word or chars.
 * @param string  | $type Choose between word and chars.
 */
if( !function_exists( 'content_excerpt' ) ): 
	function content_excerpt($text, $length, $type='words') {
		$text = strip_shortcodes( $text );
		$text = apply_filters('the_content', $text);
		$text = str_replace(']]>', ']]&gt;', $text);
		$text = strip_tags($text);
		$excerpt_length = apply_filters('excerpt_length', $length);
		$words = explode(' ', $text, $excerpt_length + 1);

		if( $type == 'chars' ) {
			$text = '';
			$testExcerpt = '';

			for( $w = 0; $w < $length; $w++ ) {
				$testExcerpt .= $words[$w] . ' ';
				$testLength = strlen($testExcerpt);
				if( $testLength <= $length ) {
					$text = $testExcerpt;
				}
				else {
					break;
				}
			}

			$text = rtrim($text);
			$text .= "...";
		}

		if( $type == 'words' ) {
			if( count($words) > $excerpt_length ) {
				array_pop($words);
				array_push($words, '');
				$text = implode(' ', $words);
			}
		}

		return $text;
	}
endif;


if( !function_exists( 'the_slug' ) ) :
	function the_slug($echo=true){
		global $post;

		if( is_singular() ){
			if( $echo == true ){
				echo ' ' . $post->post_name;
			}
			else {
				return $post->post_name;
			}
		}
	}
endif;


if( !function_exists( 'tab_data' ) ) :
	function tab_data($echo=true){
		global $post;

		if( is_singular() ){
			if( $echo == true ){
				echo $post->post_name;
			}
			else {
				return $post->post_name;
			}
		}
	}
endif;

	function wpse_allowedtags() {
    	// Add custom tags to this string
        return '<script>,<style>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>'; 
    }

if( !function_exists( 'wpse_custom_wp_trim_excerpt' ) ) : 
    function wpse_custom_wp_trim_excerpt($wpse_excerpt) {
    	$raw_excerpt = $wpse_excerpt;
        if( '' == $wpse_excerpt ) {
            $wpse_excerpt = get_the_content('');
            $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
            $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
            $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
            $wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

            //Set the excerpt word count and only break after sentence is complete.
			$excerpt_word_count = 75;
			$excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
			$tokens = array();
			$excerptOutput = '';
			$count = 0;

			// Divide the string into tokens; HTML tags, or words, followed by any whitespace
			preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

			foreach( $tokens[0] as $token ) { 
				if( $count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token) ) { 
				// Limit reached, continue until , ; ? . or ! occur at the end
					$excerptOutput .= trim($token);
					break;
				}

				// Add words to complete sentence
				$count++;

				// Append what's left of the token
				$excerptOutput .= $token;
			}

            $wpse_excerpt = trim(force_balance_tags($excerptOutput));

			$excerpt_end = ' <a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&raquo;&nbsp;' . sprintf(__( 'Read more about: %s &nbsp;&raquo;', 'wpse' ), get_the_title()) . '</a>'; 
			$excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end); 

			//$pos = strrpos($wpse_excerpt, '</');
			//if ($pos !== false)
			// Inside last HTML tag
			//$wpse_excerpt = substr_replace($wpse_excerpt, $excerpt_end, $pos, 0); /* Add read more next to last word */
			//else
			// After the content
			$wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph */

            return $wpse_excerpt;   
        }
		
        return apply_filters('wpse_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
    }
endif; 

remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'wpse_custom_wp_trim_excerpt'); 


/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load Theme options file.
 */
require get_template_directory() . '/inc/theme-options.php';

if( !function_exists('theme_option') ):
	function theme_option( $option ) {
		$options = get_option( 'theme_options' );
		if( isset( $options[$option] ) ){
			return $options[$option];
		}
		else{
			return false;
		}
	}
endif;

/**
 * Load Theme options file.
 */
require get_template_directory() . '/inc/theme-widgets.php';
