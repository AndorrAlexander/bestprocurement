<?php
	/**
	 * Template name: Industry page
	 */
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/inner', 'header-industry'); ?>

<?php get_template_part('template-parts/industry', 'content'); ?>

<?php get_template_part('template-parts/inner', 'above-footer'); ?>

<?php get_footer(); ?>




