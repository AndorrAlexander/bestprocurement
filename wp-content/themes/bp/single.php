<?php
/**
 * The template for displaying all single posts.
 *
 * @package bestprocurement
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<?php get_template_part('template-parts/inner', 'header-single'); ?>
		<section class="single-content">
		<?php while ( have_posts() ) : the_post(); ?>
			<?php get_template_part( 'template-parts/content', 'single' ); ?>
		<?php endwhile; ?>
		</section>
	</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
