<?php
	/**
	 * Template name: Team page
	 */
?>
<?php get_header(); ?>

<?php get_template_part('template-parts/inner', 'header'); ?>

<div class="wrapper">
	<?php get_template_part( 'template-parts/about', 'team' ); ?>
</div>

<?php get_template_part('template-parts/inner', 'above-footer'); ?>

<?php get_footer(); ?>