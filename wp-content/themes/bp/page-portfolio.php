<?php
	/**
	 * Template name: Portfolio page
	 */
?>

<?php
	get_header();

	$portfolio_args = array( 'post_type' => 'portfolio');
	$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
	$big = 999999999; // need an unlikely integer
	
	$portfolio_args = array(
		'post_type' => 'portfolio', 
		'paged' => $paged 
	);
	$portfolio_query = new WP_Query( $portfolio_args );

	$pagination_args = array(
		'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
		'format'             => '?page=%#%',
		'total'              => 1,
		'current'            => 0,
		'show_all'           => true,
		'end_size'           => 1,
		'mid_size'           => 2,
		'prev_next'          => true,
		'prev_text'          => __('« Previous'),
		'next_text'          => __('Next »'),
		'type'               => 'plain',
		'add_args'           => true,
		'add_fragment'       => '',
		'before_page_number' => '',
		'after_page_number'  => '',
		'current' => max( 1, get_query_var('paged') ),
		'total' => $portfolio_query->max_num_pages
	);
?>

<?php get_template_part('template-parts/inner', 'header'); ?>

	<section class="portfolio">
		<div class="wrapper">
		<?php if( $portfolio_query->have_posts() ): ?>
			<?php while( $portfolio_query->have_posts() ): $portfolio_query->the_post(); ?>
			<?php $full_image_url = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
			<article class="portfolio-item" style="background-image: url('<?php echo $full_image_url[0]; ?>')">
				<h2 class="title">
					<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
				</h2>
				<div class="mask">
					<div class="text"><?php the_excerpt(); ?></div>
				</div>
			</article>
			<?php endwhile; ?>
		<?php endif; ?>

			<div class="pagination">
			<?php echo paginate_links( $pagination_args ); ?>
			</div>
		</div>
	</section>

<?php get_template_part('template-parts/inner', 'above-footer'); ?>

<?php get_footer(); ?>




