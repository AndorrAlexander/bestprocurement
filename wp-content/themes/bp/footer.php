<?php
/**
 * The template for displaying the footer.
 *
 * @package bestprocurement
 */
?>

		</div><!-- #content .site-content-->
	</main>
	
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="wrapper">
			<div class="site-info">
			<?php if( is_active_sidebar( 'footer-description' ) ): ?>
				<div id="footer-description" class="footer-description widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer-description' ); ?>
				</div>
			<?php endif; ?>

			<?php if( is_active_sidebar( 'footer-contacts' ) ): ?>
				<div id="footer-contacts" class="footer-contacts widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer-contacts' ); ?>
				</div>
			<?php endif;  ?>
			
			<?php if( is_active_sidebar( 'footer-newsletter' ) ): ?>
				<div id="footer-newsletter" class="footer-newsletter widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer-newsletter' ); ?>
				</div>
			<?php endif; ?>

			<?php /* if( is_active_sidebar( 'footer-social' ) ): ?>
				<div id="footer-social" class="footer-social widget-area" role="complementary">
					<?php dynamic_sidebar( 'footer-social' ); ?>
				</div>
			<?php endif; */ ?>
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
