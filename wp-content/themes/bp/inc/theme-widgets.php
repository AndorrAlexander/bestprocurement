<?php

/**
 * New style declaration. Had to comment it until the PHP version on the server is upgraded to a newer version than 5.2
 * Uncomment the followings after PHP updgrade
 */
/*
add_action( 'widgets_init', function(){
	register_widget( 'BP_Widget_Recent_News' );
	register_widget( 'BP_Widget_Contacts' );
	register_widget( 'BP_Widget_Social' );
});
*/


function register_bp_widgets() {
	register_widget( 'BP_Widget_Recent_News' );
	register_widget( 'BP_Widget_Contacts' );
	register_widget( 'BP_Widget_Social' );
}
add_action( 'widgets_init', 'register_bp_widgets' );

/**
 * Recent_Posts widget class
 */
class BP_Widget_Recent_News extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget_recent_news', 'description' => __( "Your site&#8217;s most recent News.") );
		parent::__construct('recent-news', __('BP Recent News'), $widget_ops);
		$this->alt_option_name = 'widget_recent_news';

		add_action( 'save_post', 	array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	public function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_recent_news', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();

		$title = ( !empty( $instance['title'] ) ) ? $instance['title'] : __( 'BP Recent News' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( !empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 1;
		if ( !$number )
			$number = 1;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;
		$show_author = isset( $instance['show_author'] ) ? $instance['show_author'] : false;

		/**
		 * Filter the arguments for the Recent News widget.
		 *
		 * @since 3.4.0
		 *
		 * @see WP_Query::get_posts()
		 *
		 * @param array $args An array of arguments used to retrieve the recent news.
		 */
		$r = new WP_Query( apply_filters( 'widget_posts_args', array(
			'posts_per_page'      => $number,
			'post_type'			  => 'news',
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		) ) );

		if ($r->have_posts()) :
?>
		<?php echo $args['before_widget']; ?>
		<?php if ( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		} ?>
		<ul class="recent-news">
		<?php while ( $r->have_posts() ) : $r->the_post(); ?>
			<li>

			<?php if ( $show_date || $show_author ) : ?>
				<?php if ( $show_date ) : ?><span class="post-date"><?php echo get_the_date(); ?></span><?php endif; ?> | <?php if ( $show_author ) : ?><span class="post-author">by <span class="name"><?php echo get_the_author(); ?></span></span>
				<?php endif; ?>
			<?php endif; ?>

				<a class="title" href="<?php the_permalink(); ?>"><?php get_the_title() ? the_title() : the_ID(); ?></a>

				<div class="news-content"><?php get_the_excerpt() != NULL ? the_excerpt() : content_excerpt($r->post_content, 250, 'chars'); ?></div>
			</li>
		<?php endwhile; ?>
		</ul>
		<?php echo $args['after_widget']; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_recent_news', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		$instance['show_author'] = isset( $new_instance['show_author'] ) ? (bool) $new_instance['show_author'] : false;
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_news']) )
			delete_option('widget_recent_news');

		return $instance;
	}

	public function flush_widget_cache() {
		wp_cache_delete('widget_recent_news', 'widget');
	}

	public function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 1;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
		$show_author = isset( $instance['show_author'] ) ? (bool) $instance['show_author'] : false;
?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of news to show:' ); ?></label>
			<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" disabled />
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?' ); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_author ); ?> id="<?php echo $this->get_field_id( 'show_author' ); ?>" name="<?php echo $this->get_field_name( 'show_author' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_author' ); ?>"><?php _e( 'Display author?' ); ?></label>
		</p>
<?php
	}
}


/**
 * Contacts widget class
 */
class BP_Widget_Contacts extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget_contacts', 'description' => __( "Your site&#8217;s Contact credentials.") );
		parent::__construct('contacts', __('BP Contacts'), $widget_ops);
		$this->alt_option_name = 'widget_contacts';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	public function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_contacts', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();

		$title = ( !empty( $instance['title'] ) ) ? $instance['title'] : __( 'BP Contacts' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$show_contact_location = isset( $instance['show_contact_location'] ) ? $instance['show_contact_location'] : false;
		$show_contact_email = isset( $instance['show_contact_email'] ) ? $instance['show_contact_email'] : false;
		$show_contact_phone = isset( $instance['show_contact_phone'] ) ? $instance['show_contact_phone'] : false;
		$show_contact_fax = isset( $instance['show_contact_fax'] ) ? $instance['show_contact_fax'] : false;

		echo $args['before_widget'];
		if( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
?>
		<div class="contacts">
		<?php if( $show_contact_location ): ?>
			<span class="location"><?php echo theme_option( 'contact_address' ); ?></span>
		<?php endif; ?>

		<?php if( $show_contact_email ): ?>
			<span class="email"><?php echo theme_option( 'contact_email' ); ?></span>
		<?php endif; ?>

		<?php if( $show_contact_phone || $show_contact_fax ): ?>
			<div class="phone-fax">
			<?php if( $show_contact_phone ): ?>
				<span class="phone"><?php echo theme_option( 'contact_phone' ); ?></span>
			<?php endif; ?>

			<?php if( $show_contact_fax ): ?>
				<span class="fax"><?php echo theme_option( 'contact_fax' ); ?></span>
			<?php endif; ?>
			</div>
		<?php endif; ?>
		</div>

		<?php echo $args['after_widget']; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_contacts', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['show_contact_location'] = isset( $new_instance['show_contact_location'] ) ? (bool) $new_instance['show_contact_location'] : false;
		$instance['show_contact_email'] = isset( $new_instance['show_contact_email'] ) ? (bool) $new_instance['show_contact_email'] : false;
		$instance['show_contact_phone'] = isset( $new_instance['show_contact_phone'] ) ? (bool) $new_instance['show_contact_phone'] : false;
		$instance['show_contact_fax'] = isset( $new_instance['show_contact_fax'] ) ? (bool) $new_instance['show_contact_fax'] : false;

		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if( isset($alloptions['widget_contacts']) )
			delete_option('widget_contacts');

		return $instance;
	}

	public function flush_widget_cache() {
		wp_cache_delete('widget_contacts', 'widget');
	}

	public function form( $instance ) {
		$title     					= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$show_contact_location     	= isset( $instance['show_contact_location'] ) ? (bool)( $instance['show_contact_location'] ) : '';
		$show_contact_email     	= isset( $instance['show_contact_email'] ) ? (bool)( $instance['show_contact_email'] ) : '';
		$show_contact_phone     	= isset( $instance['show_contact_phone'] ) ? (bool)( $instance['show_contact_phone'] ) : '';
		$show_contact_fax     		= isset( $instance['show_contact_fax'] ) ? (bool)( $instance['show_contact_fax'] ) : '';
?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_contact_location ); ?> id="<?php echo $this->get_field_id( 'show_contact_location' ); ?>" name="<?php echo $this->get_field_name( 'show_contact_location' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_contact_location' ); ?>"><?php _e( 'Display Contact address?' ); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_contact_email ); ?> id="<?php echo $this->get_field_id( 'show_contact_email' ); ?>" name="<?php echo $this->get_field_name( 'show_contact_email' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_contact_email' ); ?>"><?php _e( 'Display Contact email?' ); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_contact_phone ); ?> id="<?php echo $this->get_field_id( 'show_contact_phone' ); ?>" name="<?php echo $this->get_field_name( 'show_contact_phone' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_contact_phone' ); ?>"><?php _e( 'Display Contact phone?' ); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_contact_fax ); ?> id="<?php echo $this->get_field_id( 'show_contact_fax' ); ?>" name="<?php echo $this->get_field_name( 'show_contact_fax' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_contact_fax' ); ?>"><?php _e( 'Display Contact fax' ); ?></label>
		</p>
<?php
	}
}


/**
 * Social widget class
 */
class BP_Widget_Social extends WP_Widget {

	public function __construct() {
		$widget_ops = array('classname' => 'widget_social', 'description' => __( "Your site&#8217;s Social credentials.") );
		parent::__construct('social', __('BP Social'), $widget_ops);
		$this->alt_option_name = 'widget_social';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	public function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_social', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();

		$title = ( !empty( $instance['title'] ) ) ? $instance['title'] : __( 'BP Social' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$show_facebook = isset( $instance['show_facebook'] ) ? $instance['show_facebook'] : false;
		$show_twitter = isset( $instance['show_twitter'] ) ? $instance['show_twitter'] : false;
		$show_linkedin = isset( $instance['show_linkedin'] ) ? $instance['show_linkedin'] : false;
		$show_googleplus = isset( $instance['show_googleplus'] ) ? $instance['show_googleplus'] : false;

		echo $args['before_widget'];
		if( $title ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
?>
		<div class="social">
		<?php if( $show_facebook ): ?>
			<a class="facebook" onclick="target='_blank'" href="<?php echo theme_option( 'facebook' ); ?>" title="Facebook">Facebook</a>
		<?php endif; ?>

		<?php if( $show_twitter ): ?>
			<a class="twitter" onclick="target='_blank'" href="<?php echo theme_option( 'twitter' ); ?>" title="Twitter">Twitter</a>
		<?php endif; ?>

		<?php if( $show_linkedin ): ?>
			<a class="linkedin" onclick="target='_blank'" href="<?php echo theme_option( 'linkedin' ); ?>" title="LinkedIn">LinkedIn</a>
		<?php endif; ?>

		<?php if( $show_googleplus ): ?>
			<a class="googleplus" onclick="target='_blank'" href="<?php echo theme_option( 'googleplus' ); ?>" title="GooglePlus">GooglePlus</a>
		<?php endif; ?>
		</div>

		<?php echo $args['after_widget']; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_social', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['show_facebook'] = isset( $new_instance['show_facebook'] ) ? (bool) $new_instance['show_facebook'] : false;
		$instance['show_twitter'] = isset( $new_instance['show_twitter'] ) ? (bool) $new_instance['show_twitter'] : false;
		$instance['show_linkedin'] = isset( $new_instance['show_linkedin'] ) ? (bool) $new_instance['show_linkedin'] : false;
		$instance['show_googleplus'] = isset( $new_instance['show_googleplus'] ) ? (bool) $new_instance['show_googleplus'] : false;

		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if( isset($alloptions['widget_social']) )
			delete_option('widget_social');

		return $instance;
	}

	public function flush_widget_cache() {
		wp_cache_delete('widget_social', 'widget');
	}

	public function form( $instance ) {
		$title     			= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$show_facebook     	= isset( $instance['show_facebook'] ) ? (bool)( $instance['show_facebook'] ) : '';
		$show_twitter     	= isset( $instance['show_twitter'] ) ? (bool)( $instance['show_twitter'] ) : '';
		$show_linkedin    	= isset( $instance['show_linkedin'] ) ? (bool)( $instance['show_linkedin'] ) : '';
		$show_googleplus	= isset( $instance['show_googleplus'] ) ? (bool)( $instance['show_googleplus'] ) : '';
?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_facebook ); ?> id="<?php echo $this->get_field_id( 'show_facebook' ); ?>" name="<?php echo $this->get_field_name( 'show_facebook' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_facebook' ); ?>"><?php _e( 'Display Facebook social icon?' ); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_twitter ); ?> id="<?php echo $this->get_field_id( 'show_twitter' ); ?>" name="<?php echo $this->get_field_name( 'show_twitter' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_twitter' ); ?>"><?php _e( 'Display Twitter social icon?' ); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_linkedin ); ?> id="<?php echo $this->get_field_id( 'show_linkedin' ); ?>" name="<?php echo $this->get_field_name( 'show_linkedin' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_linkedin' ); ?>"><?php _e( 'Display LinkedIn social icon?' ); ?></label>
		</p>

		<p>
			<input class="checkbox" type="checkbox" <?php checked( $show_googleplus ); ?> id="<?php echo $this->get_field_id( 'show_googleplus' ); ?>" name="<?php echo $this->get_field_name( 'show_googleplus' ); ?>" />
			<label for="<?php echo $this->get_field_id( 'show_googleplus' ); ?>"><?php _e( 'Display GooglePlus social icon?' ); ?></label>
		</p>
<?php
	}
}
