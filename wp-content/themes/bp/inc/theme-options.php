<?php

class Bp_Theme_Options {

	/* Array of sections for the theme options page */
	private $sections;
	private $checkboxes;
	private $settings;

	/* Initialize */
	function __construct() {
		// This will keep track of the checkbox options for the validate_settings function.
		$this->checkboxes = array();
		$this->settings = array();
		$this->get_settings();

		$this->sections['general']      = __( 'General Settings', 'bp' );
		$this->sections['social']       = __( 'Social Settings', 'bp' );
		$this->sections['about']        = __( 'About', 'bp' );

		add_action( 'admin_menu', array( &$this, 'add_pages' ) );
		add_action( 'admin_init', array( &$this, 'register_settings' ) );

		if( !get_option( 'theme_options' ) ){
			$this->initialize_settings();
		}
	}

	/* Add page(s) to the admin menu */
	public function add_pages() {

		$admin_page = add_menu_page( 'BP Options', 'BP Options', 'manage_options', 'theme-options', array( &$this, 'display_page' ), 'dashicons-admin-generic', 3 );

		add_action( 'admin_print_scripts-' . $admin_page, array( &$this, 'scripts' ) );
		add_action( 'admin_print_styles-' . $admin_page, array( &$this, 'styles' ) );

	}

	/* Create settings field */
	public function create_setting( $args = array() ) {

		$defaults = array(
			'id'      => 'default_field',
			'title'   => 'Default Field',
			'desc'    => 'This is a default description.',
			'std'     => '',
			'type'    => 'text',
			'section' => 'general',
			'choices' => array(),
			'class'   => ''
		);

		extract( wp_parse_args( $args, $defaults ) );

		$field_args = array(
			'type'      => $type,
			'id'        => $id,
			'desc'      => $desc,
			'std'       => $std,
			'choices'   => $choices,
			'label_for' => $id,
			'class'     => $class
		);

		if( $type == 'checkbox' ){
			$this->checkboxes[] = $id;
		}

		add_settings_field( $id, $title, array( $this, 'display_setting' ), 'theme-options', $section, $field_args );

	}

	/* HTML to display the theme options page */
	public function display_page() {

		echo '<div class="wrap">
			<h2>' . __( 'Best Procurment Theme Options', 'bp' ) . '</h2>';

				if( isset( $_GET['settings-updated'] ) && $_GET['settings-updated'] == true ) {
					echo '<div class="updated fade"><p>' . __( 'Theme options updated.', 'bp' ) . '</p></div>';
				}

				echo '<form action="options.php" method="post">';

				settings_fields( 'theme_options' );

				echo '<div class="ui-tabs">';
				echo '<ul class="ui-tabs-nav">';

				foreach( $this->sections as $section_slug => $section ) {
					echo '<li><a href="#' . $section_slug . '">' . $section . '</a></li>';
				}

				echo '</ul>';

				do_settings_sections( $_GET['page'] );

				echo '</div>';

				submit_button();

				echo '</form>';

			echo '<script type="text/javascript">
				jQuery(document).ready(function($) {
					var sections = [];';

					foreach( $this->sections as $section_slug => $section )
						echo "sections['$section'] = '$section_slug';";

					echo 'var wrapped = $(".wrap h3").wrap("<div class=\"ui-tabs-panel\">");
					wrapped.each(function() {
						$(this).parent().append($(this).parent().nextUntil("div.ui-tabs-panel"));
					});
					$(".ui-tabs-panel").each(function(index) {
						$(this).attr("id", sections[$(this).children("h3").text()]);
						if (index > 0)
							$(this).addClass("ui-tabs-hide");
					});
					$(".ui-tabs").tabs({
						fx: { opacity: "toggle", duration: "fast" }
					});

					$("input[type=text], textarea").each(function() {
						if ($(this).val() == $(this).attr("placeholder") || $(this).val() == "")
							$(this).css("color", "#999");
					});

					$("input[type=text], textarea").focus(function() {
						if ($(this).val() == $(this).attr("placeholder") || $(this).val() == "") {
							$(this).val("");
							$(this).css("color", "#000");
						}
					}).blur(function() {
						if ($(this).val() == "" || $(this).val() == $(this).attr("placeholder")) {
							$(this).val($(this).attr("placeholder"));
							$(this).css("color", "#999");
						}
					});

					$(".wrap h3, .wrap table").show();

					// This will make the "warning" checkbox class really stand out when checked.
					// I use it here for the Reset checkbox.
					$(".warning").change(function() {
						if ($(this).is(":checked"))
							$(this).parent().css("background", "#c00").css("color", "#fff").css("fontWeight", "bold");
						else
							$(this).parent().css("background", "none").css("color", "inherit").css("fontWeight", "normal");
					});

					// Browser compatibility
					if ($.browser.mozilla)
							 $("form").attr("autocomplete", "off");
				});
			</script>
		</div>';
	}

	/**
	 * Description for section
	 */
	public function display_section() {
		return true;
	}

	/**
	 * Description for About section
	 */
	public function display_about_section() {
		$about = '<img src="http://www.brandup.ro/img/brandUp.png" alt="">';
		$about .= '<p>&copy; Copyright ' . date('Y') . ' BrandUP <a href="http://www.brandup.ro" target="_blank">brandup.ro</a></p>';

		echo $about;
	}


	public function display_setting( $args = array() ) {

		extract( $args );

		$options = get_option( 'theme_options' );

		if( !isset( $options[$id] ) && $type != 'checkbox' ) {
			$options[$id] = $std;
		}
		elseif( !isset( $options[$id] ) ) {
			$options[$id] = 0;
		}

		$field_class = '';
		if( $class != '' ){
			$field_class = ' ' . $class;
		}

		switch( $type ) {

			case 'heading':
				echo '</td></tr><tr valign="top"><td colspan="2"><h2>' . $desc . '</h2>';
				break;

			case 'checkbox':
				echo '<input class="checkbox' . $field_class . '" type="checkbox" id="' . $id . '" name="theme_options[' . $id . ']" value="1" ' . checked( $options[$id], 1, false ) . ' /> <label for="' . $id . '">' . $desc . '</label>';

				break;

			case 'select':
				echo '<select class="select' . $field_class . '" name="theme_options[' . $id . ']">';

				foreach( $choices as $value => $label ) {
					echo '<option value="' . esc_attr( $value ) . '"' . selected( $options[$id], $value, false ) . '>' . $label . '</option>';
				}

				echo '</select>';

				if( $desc != '' ){
					echo '<br /><span class="description">' . $desc . '</span>';
				}

				break;

			case 'radio':
				$i = 0;
				foreach( $choices as $value => $label ) {
					echo '<input class="radio' . $field_class . '" type="radio" name="theme_options[' . $id . ']" id="' . $id . $i . '" value="' . esc_attr( $value ) . '" ' . checked( $options[$id], $value, false ) . '> <label for="' . $id . $i . '">' . $label . '</label>';
					if( $i < count( $options ) - 1 ) {
						echo '<br />';
					}
					$i++;
				}

				if( $desc != '' ){
					echo '<br /><span class="description">' . $desc . '</span>';
				}

				break;

			case 'textarea':
				echo '<textarea class="' . $field_class . '" id="' . $id . '" name="theme_options[' . $id . ']" placeholder="' . $std . '" rows="5" cols="30">' . format_for_editor( $options[$id] ) . '</textarea>';

				if( $desc != '' ) {
					echo '<br /><span class="description">' . $desc . '</span>';
				}

				break;

			case 'password':
				echo '<input class="' . $field_class . '" type="password" id="' . $id . '" name="theme_options[' . $id . ']" value="' . esc_attr( $options[$id] ) . '" />';

				if( $desc != '' ) {
					echo '<br /><span class="description">' . $desc . '</span>';
				}

				break;

			case 'text':
				default:
					echo '<input class="' . $field_class . '" type="text" id="' . $id . '" name="theme_options[' . $id . ']" placeholder="' . $std . '" value="' . esc_attr( $options[$id] ) . '" />';

				if( $desc != '' ){
					echo '<br /><span class="description">' . $desc . '</span>';
				}

				break;
		}

	}

	/* Define all settings and their defaults */
	public function get_settings() {

		/* General Settings */

		$this->settings['homepage_description'] = array(
			'title'   => __( 'Homepage Description', 'bp' ),
			'desc'    => __( 'This text will appear on the homepage, under the carousel.', 'bp' ),
			'std'     => 'Enter text here',
			'type'    => 'textarea',
			'section' => 'general',
			'class'	  => 'widefat',
		);

		$this->settings['contact_credentials'] = array(
			'title'   => '',
			'desc'    => __( 'Contact Details:', 'bp' ),
			'type'    => 'heading',
			'section' => 'general',
		);

		$this->settings['contact_address'] = array(
			'title'   => __( 'Address', 'bp' ),
			'desc'    => __( 'Contact address', 'bp' ),
			'std'     => 'Enter contact postal address here',
			'type'    => 'text',
			'section' => 'general',
			'class'	  => 'widefat',
		);

		$this->settings['contact_email'] = array(
			'title'   => __( 'Email', 'bp' ),
			'desc'    => __( 'Email address', 'bp' ),
			'std'     => 'Enter e-mail address here',
			'type'    => 'text',
			'section' => 'general',
			'class'	  => 'widefat',
		);

		$this->settings['contact_phone'] = array(
			'title'   => __( 'Phone', 'bp' ),
			'desc'    => __( 'Contact phone', 'bp' ),
			'std'     => 'Enter phone number here',
			'type'    => 'text',
			'section' => 'general',
			'class'	  => 'regular-text',
		);

		$this->settings['contact_fax'] = array(
			'title'   => __( 'Fax', 'bp' ),
			'desc'    => __( 'Contact fax', 'bp' ),
			'std'     => 'Enter fax number here',
			'type'    => 'text',
			'section' => 'general',
			'class'	  => 'regular-text',
		);


		/* Social Settings */

		$this->settings['facebook'] = array(
			'title'   => __( 'Facebook URL', 'bp' ),
			'desc'    => __( 'Please insert the Facebook address.', 'bp' ),
			'std'     => 'Enter url here',
			'type'    => 'text',
			'section' => 'social',
			'class'	  => 'widefat',
		);
		$this->settings['twitter'] = array(
			'title'   => __( 'Twitter URL', 'bp' ),
			'desc'    => __( 'Please insert the Twitter address.', 'bp' ),
			'std'     => 'Enter url here',
			'type'    => 'text',
			'section' => 'social',
			'class'	  => 'widefat',
		);
		$this->settings['linkedin'] = array(
			'title'   => __( 'LinkedIn URL', 'bp' ),
			'desc'    => __( 'Please insert the LinkedIn address.', 'bp' ),
			'std'     => 'Enter url here',
			'type'    => 'text',
			'section' => 'social',
			'class'	  => 'widefat',
		);
		$this->settings['googleplus'] = array(
			'title'   => __( 'Google+ URL', 'bp' ),
			'desc'    => __( 'Please insert the Google+ address.', 'bp' ),
			'std'     => 'Enter url here',
			'type'    => 'text',
			'section' => 'social',
			'class'	  => 'widefat',
		);
	}

	/* Initialize settings to their default values */
	public function initialize_settings() {

		$default_settings = array();
		foreach( $this->settings as $id => $setting ) {
			if( $setting['type'] != 'heading' ) {
				$default_settings[$id] = $setting['std'];
			}
		}

		update_option( 'theme_options', $default_settings );

	}

	/* Register settings via the WP Settings API */
	public function register_settings() {

		register_setting( 'theme_options', 'theme_options', array( &$this, 'validate_settings' ) );

		foreach( $this->sections as $slug => $title ) {
			if( $slug == 'about' ) {
				add_settings_section( $slug, $title, array( &$this, 'display_about_section' ), 'theme-options' );
			}
			else{
				add_settings_section( $slug, $title, array( &$this, 'display_section' ), 'theme-options' );
			}
		}

		$this->get_settings();

		foreach( $this->settings as $id => $setting ) {
			$setting['id'] = $id;
			$this->create_setting( $setting );
		}

	}

	/**
	* jQuery Tabs
	*/
	public function scripts() {
		wp_print_scripts( 'jquery-ui-tabs' );
	}

	/**
	* Styling for the theme options page
	*/
	public function styles() {
		wp_register_style( 'theme-admin', get_bloginfo( 'stylesheet_directory' ) . '/layouts/theme-options.css' );
		wp_enqueue_style( 'theme-admin' );
	}

	/**
	* Validate settings
	*/
	public function validate_settings( $input ) {

		if( !isset( $input['reset_theme'] ) ) {
			$options = get_option( 'theme_options' );

			foreach( $this->checkboxes as $id ) {
				if( isset( $options[$id] ) && !isset( $input[$id] ) ) {
					unset( $options[$id] );
				}
			}

			return $input;
		}

		return false;
	}
}

$theme_options = new Bp_Theme_Options();


