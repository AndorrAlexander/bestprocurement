<?php
	/**
	 * Template name: Contact page
	 */
?>

<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="contact-page">
				<div class="map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2847.5870659820853!2d26.061091115773102!3d44.462137907762276!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x40b20221a3c7adef%3A0xd9575c7a3be73eb!2sStrada+Elena+Clucereasa+20%2C+Bucure%C8%99ti+011553!5e0!3m2!1sen!2sro!4v1446391833735" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				
				<div class="wrapper">
					<div class="contact-details">
						<div class="contact headquarters">
							<h3 class="title"><?php _e('Our Headquarter','bp'); ?></h3>
							<div class="desc"><?php echo theme_option( 'contact_address' ); ?></div>
						</div>

						<div class="contact support">
							<h3 class="title"><?php _e('Contact Us','bp'); ?></h3>
							<div class="desc">
								<p><?php echo theme_option( 'contact_phone' ); ?></p>
								<p><?php echo theme_option( 'contact_email' ); ?></p>
							</div>
						</div>
					</div>

					<div class="contact-form">
						<h3 class="title">Get More Info, <span class="quote">Request a Quote</span> or Just Say Hi</h3>
						<?php echo do_shortcode('[contact-form-7 id="22" title="BP - Contact form"]'); ?>
					</div>
				</div>
			</div>

		</main>
	</div>

<?php get_footer(); ?>
