<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'bp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+]u!Ug_$>{o<W{@c|Iazp+A|GF&P]2Ezn<_,^7>+*R0#o+35EZ*JKr5^O:*lLo+|');
define('SECURE_AUTH_KEY',  'ol#i-Oz6|Y877X+>6 Y[ JA|87WhU6Qj&$+-}nyr,n3/L&B]3IM[Si9*CW0Ji$_G');
define('LOGGED_IN_KEY',    'AqUOYW{WjJAR`3@VdYYf|hV gNLcvErtuK+Pm;K+9HKofE+{/geuBDZo5~mZwYx:');
define('NONCE_KEY',        'HCKv7Q-Nq5L@!0Vu-Qa*XJt+H/]-Ub.m%c1Lg(t6|QvxnJ/xfuL>;?~!Hn9L*6-x');
define('AUTH_SALT',        '@FMK}(Odl^-@XwGL.f-[T>lML{Q|pvTt.Ot5wP/fTY{JXVD;-3edEsS_07XpX~WU');
define('SECURE_AUTH_SALT', 'fWBQn2|T?vzhoXbA-b]el<9AB;B|fxFL`Klh5V8Na!P3Kmu)anYU#7dMdG&!yo,8');
define('LOGGED_IN_SALT',   'T]2|H$?rjG|d~7_OhM[pge6R~dik2}G{tg<1NG0HtC|P|Kx,aw=I*(*US@Ba4wV[');
define('NONCE_SALT',       'tv3oynP*N.e%[($|nCx<^DAgldkuw|D(!lylgAIfgzc$F@UYK]%rMeEGSLKYP+w6');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'bp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
